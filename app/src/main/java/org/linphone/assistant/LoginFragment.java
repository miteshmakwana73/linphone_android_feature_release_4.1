package org.linphone.assistant;
/*
LoginFragment.java
Copyright (C) 2017  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import org.linphone.R;
import org.linphone.core.TransportType;
import org.linphone.jsonurl.Config;

public class LoginFragment extends Fragment implements OnClickListener, TextWatcher {
    private EditText mLogin, mUserid, mPassword, mDomain, mDisplayName;
    private RadioGroup mTransports;
    private Button mApply;

    private TextView tvTitle;

    String URL_LOGIN = Config.LOGIN_URL;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.assistant_login, container, false);

        tvTitle = (TextView) view.findViewById(R.id.tvtitle);
        mLogin = view.findViewById(R.id.assistant_username);
        mLogin.addTextChangedListener(this);
        mDisplayName = view.findViewById(R.id.assistant_display_name);
        mDisplayName.addTextChangedListener(this);
        mUserid = view.findViewById(R.id.assistant_userid);
        mUserid.addTextChangedListener(this);
        mPassword = view.findViewById(R.id.assistant_password);
        mPassword.addTextChangedListener(this);
        mDomain = view.findViewById(R.id.assistant_domain);
        mDomain.addTextChangedListener(this);
        mTransports = view.findViewById(R.id.assistant_transports);
        mApply = view.findViewById(R.id.assistant_apply);
        mApply.setEnabled(false);
        mApply.setOnClickListener(this);

        Shader myShader =
                new LinearGradient(
                        100,
                        0,
                        0,
                        40,
                        Color.parseColor("#59C6EB"),
                        Color.parseColor("#73D99B"),
                        Shader.TileMode.CLAMP);
        tvTitle.getPaint().setShader(myShader);

        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.assistant_apply) {
            if (mLogin.getText() == null
                    || mLogin.length() == 0
                    || mPassword.getText() == null
                    || mPassword.length() == 0
                    || mDomain.getText() == null
                    || mDomain.length() == 0) {
                Toast.makeText(
                                getActivity(),
                                getString(R.string.first_launch_no_login_password),
                                Toast.LENGTH_LONG)
                        .show();
                return;
            }

            TransportType transport;
            if (mTransports.getCheckedRadioButtonId() == R.id.transport_udp) {
                transport = TransportType.Udp;
            } else {
                if (mTransports.getCheckedRadioButtonId() == R.id.transport_tcp) {
                    transport = TransportType.Tcp;
                } else {
                    transport = TransportType.Tls;
                }
            }
            loginCheck(transport);

            /*if (mDomain.getText().toString().compareTo(getString(R.string.default_domain)) == 0) {
                AssistantActivity.instance()
                        .displayLoginLinphone(
                                mLogin.getText().toString(), mPassword.getText().toString());
            } else {
                AssistantActivity.instance()
                        .genericLogIn(
                                mLogin.getText().toString(),
                                mUserid.getText().toString(),
                                mPassword.getText().toString(),
                                mDisplayName.getText().toString(),
                                null,
                                mDomain.getText().toString(),
                                transport);
            }*/
        }
    }

    public void loginCheck(final TransportType transport) {
        // Creating string request with post method.
        StringRequest stringRequest =
                new StringRequest(
                        Request.Method.POST,
                        URL_LOGIN,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String ServerResponse) {

                                // Hiding the progress dialog after all task complete.
                                //                        progressBar.setVisibility(View.GONE);
                                try {
                                    JSONObject jobj = new JSONObject(ServerResponse);

                                    String status = jobj.getString("success");

                                    String msg = jobj.getString("message");

                                    if (status.equals("true")) {
                                        // move to next page

                                        JSONObject obj = new JSONObject(ServerResponse);

                                        JSONObject jsonobject = obj.getJSONObject("data");

                                        String currency = null, balance = null, token = null;

                                        currency = jsonobject.getString("currency");
                                        balance = jsonobject.getString("balance");
                                        token = jsonobject.getString("token");
                                        // Log.e("Data",ServerResponse);

                                        /*for(int i=0; i < heroarray.length(); i++) {
                                            JSONObject jsonobject = heroarray.getJSONObject(i);
                                            currency=jsonobject.getString("currency");
                                            balance=jsonobject.getString("balance");
                                            token=jsonobject.getString("token");
                                        }*/

                                        SharedPreferences sharedPreferences =
                                                getActivity().getSharedPreferences("MyFile", 0);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("currency", currency);
                                        editor.putString("balance", balance);
                                        editor.putString("token", token);
                                        //
                                        // editor.putInt("loginstatus",1);
                                        editor.commit();

                                        String username = mLogin.getText().toString().trim();
                                        String userid = mUserid.getText().toString().trim();
                                        String pass = mPassword.getText().toString().trim();
                                        String dis = mDisplayName.getText().toString().trim();

                                        // TODO Start calling the login interface
                                        AssistantActivity.instance()
                                                .genericLogIn(
                                                        username,
                                                        userid,
                                                        pass,
                                                        dis,
                                                        null,
                                                        mDomain.getText().toString(),
                                                        transport);
                                        Log.e("msg", msg);
                                        showToast(msg);
                                    } else {
                                        showToast(msg);
                                    }

                                    // Toast.makeText(SignupActivity.this, ServerResponse,
                                    // Toast.LENGTH_LONG).show();
                                    Log.e("success", msg);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                // Showing response message coming from server.

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {

                                // Hiding the progress dialog after all task complete.
                                //                        progressBar.setVisibility(View.GONE);

                                if (volleyError instanceof NoConnectionError) {
                                    showToast("No connection available");
                                } else if (volleyError.getClass().equals(TimeoutError.class)) {
                                    // Show timeout error message
                                    showToast("Oops. Timeout error!");
                                } else if (volleyError instanceof ServerError) {
                                    // Show timeout error message
                                    showToast("Server error!");
                                } else {
                                    showToast("Something Wrong");
                                }
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("x-api-key", Config.DEFAULT_LOGIN_KEY);

                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() {

                        // Creating Map String Params.
                        Map<String, String> params = new HashMap<String, String>();

                        String username = mLogin.getText().toString().trim();
                        String pass = mPassword.getText().toString().trim();
                        // Adding All values to Params.
                        params.put("username", username);
                        params.put("password", pass);

                        return params;
                    }
                };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mApply.setEnabled(
                !mLogin.getText().toString().isEmpty()
                        && !mPassword.getText().toString().isEmpty()
                        && !mDomain.getText().toString().isEmpty());
    }

    @Override
    public void afterTextChanged(Editable s) {}
}
